[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![Pipeline Status](https://gitlab.com/tawasta/odoo/social/badges/17.0-dev/pipeline.svg)](https://gitlab.com/tawasta/odoo/social/-/pipelines/)

Social
======
Odoo Social Addons

[//]: # (addons)

Available addons
----------------
addon | version | maintainers | summary
--- | --- | --- | ---
[mail_attach_product_attachment](mail_attach_product_attachment/) | 17.0.1.0.0 |  | Use SO/PO line product attachments in mail compose
[mail_autosubscribe_notify_bypass](mail_autosubscribe_notify_bypass/) | 17.0.1.0.0 |  | Model-specific way to not send autosubscribe mails
[mail_custom_headers](mail_custom_headers/) | 17.0.1.0 |  | Allow using custom headers when sending email
[mail_template_name_translatable](mail_template_name_translatable/) | 17.0.1.0 |  | Make mail template name a translatable field
[mail_toggleable_security_setting_update_email](mail_toggleable_security_setting_update_email/) | 17.0.1.0.0 |  | Adds a setting to toggle off notifications about changed user login, email or password
[mass_mailing_list_sync_automation](mass_mailing_list_sync_automation/) | 17.0.1.0.0 |  | Mass Mailing List Sync Automation
[mass_mailing_multi_company](mass_mailing_multi_company/) | 17.0.1.0.0 |  | Mass mailing multi company
[mass_mailing_privacy_consent](mass_mailing_privacy_consent/) | 17.0.1.0.0 |  | Mass mailing privacy consent
[mass_mailing_queue_send](mass_mailing_queue_send/) | 17.0.1.0.0 |  | Send mass mailing emails with queue
[mass_mailing_subject_not_translatable](mass_mailing_subject_not_translatable/) | 17.0.1.0.0 |  | Mass mailing subject not translatable
[mass_mailing_unsubscribe_custom_template](mass_mailing_unsubscribe_custom_template/) | 17.0.1.0.0 |  | Custom template when unsubscribing from a mass mail
[privacy_consent_fields](privacy_consent_fields/) | 17.0.1.0.0 |  | Privacy consent fields - Activity and Subject

[//]: # (end addons)
